var serviceModule = angular.module('serviceModule', []);

serviceModule.controller("AngularJSPostController", function($scope, $http) {
	
		$scope.variable = "AngularJS POST Spring MVC Example:";	
		
		var dataObj = '{"categoria" : "Cocina"}';	
		
		var response = $http.post('rest/tableros/listar/', dataObj);
		response.success(function(data, status, headers, config) {
			$scope.responseData = data;
		});
		response.error(function(data, status, headers, config) {
			alert( "Exception details: " + JSON.stringify({data: data}));
		});
	
});