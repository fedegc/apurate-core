package ar.com.lemondata.apurate.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import ar.com.lemondata.apurate.mapper.BaseMapper;
import ar.com.lemondata.apurate.service.GenericService;

@Controller
public class BaseController {
	/**
	 * Inyectamos el GenericService
	 */
	@Resource
	protected GenericService genericService;
	
	/**
	 * Referencia a un mapeador para mapear datos de solicitud y respuesta
	 */
	
	protected BaseMapper mapper;
	
	
	public void setGenericService(GenericService genericService) {
		this.genericService = genericService;
	}
}
