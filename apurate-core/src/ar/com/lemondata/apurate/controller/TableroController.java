package ar.com.lemondata.apurate.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ar.com.lemondata.apurate.datatransfer.BaseDTO;
import ar.com.lemondata.apurate.datatransfer.MatrizDTO;
import ar.com.lemondata.apurate.datatransfer.TableroDTO;
import ar.com.lemondata.apurate.datatransfer.request.TableroDTORequest;
import ar.com.lemondata.apurate.datatransfer.response.ResponseWSDTO;
import ar.com.lemondata.apurate.datatransfer.response.TableroDTOResponse;
import ar.com.lemondata.apurate.mapper.TableroMapper;
import ar.com.lemondata.apurate.utillity.ConstantesApurate;
import ar.com.lemondata.apurate.utillity.ConstantesBusqueda;

@Controller
@RequestMapping("/tableros")
public class TableroController extends BaseController {

	
	@RequestMapping(value = "/listar/", method = RequestMethod.POST)
	public @ResponseBody ResponseWSDTO getTableros (@RequestBody final TableroDTORequest tableroRequest){
		mapper = new TableroMapper();
		ConstantesBusqueda.matchLike = true;
		ResponseWSDTO respuesta = new ResponseWSDTO();

		try {
			// creo el filtro y busco
			TableroDTO filtro = (TableroDTO) mapper.mapRequest(tableroRequest);
			filtro.setMatriz(new MatrizDTO());
			List<BaseDTO> tablerosDTO = (List<BaseDTO>)genericService.findAllByOrder(filtro, "id", "ASC");
			Iterator<BaseDTO> iterator = tablerosDTO.iterator();
			
			// cast a dto response y agrego en una lista
			List<TableroDTOResponse> listaTableros = new ArrayList<TableroDTOResponse>();
			while (iterator.hasNext()) {
				TableroDTO tableroDTO = (TableroDTO) iterator.next();
				listaTableros.add((TableroDTOResponse) mapper.mapResponse(tableroDTO));
			}

			// agrego la lista a la respuesta y codigo de estado
			respuesta.setCodigo(ConstantesApurate.RESULTADO_EXITO);
			respuesta.setObjeto(listaTableros);
		} catch (Exception e) {
			respuesta.setCodigo(ConstantesApurate.RESULTADO_ERROR);
			respuesta.setMensaje("Ocurri� un error al obtener el listado.");
		}

		respuesta.setMetodo("tableros/listar");
		return respuesta;
	}
}
