package ar.com.lemondata.apurate.dao;

import ar.com.lemondata.apurate.persistencia.BaseEntity;

public interface GenericDAO extends IDAO<BaseEntity> {

}
