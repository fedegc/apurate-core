package ar.com.lemondata.apurate.dao;

import java.sql.Connection;
import java.util.List;

import ar.com.lemondata.apurate.persistencia.BaseEntity;
import ar.com.lemondata.apurate.utillity.PaginationResult;

public interface IDAO<T extends BaseEntity> {
	void persist(T entity);

	void persist(List<T> entities);

	T update(T entity);

	T findById(Long id);

	List<T> findAll(T entity);

	List<T> findAllByOrder(T entity, String campo, String order);

	T findOne(T entity);

	@SuppressWarnings("hiding")
	<T> List<T> findByHQL(String hql);

	void remove(T entity);

	void removeById(Long entityId);

	List<T> findByExample(T exampleEntity);

	PaginationResult<T> consultarPorFiltrosConPaginacion(T exampleEntity,
			int index, int offset);

	void setClazz(Class<T> clazz);

	Connection getConnection();

}
