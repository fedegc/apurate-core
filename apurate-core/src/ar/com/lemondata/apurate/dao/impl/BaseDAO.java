package ar.com.lemondata.apurate.dao.impl;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import ar.com.lemondata.apurate.dao.IDAO;
import ar.com.lemondata.apurate.persistencia.BaseEntity;
import ar.com.lemondata.apurate.utillity.ConstantesBusqueda;
import ar.com.lemondata.apurate.utillity.PaginationResult;

public class BaseDAO <T extends BaseEntity> implements IDAO<T> {
	/**
	 * Clase gen�rica sobre la que trabajar� el DAO
	 */

	private Class<T> clazz;

	/**
	 * S�lo debe poder setearse
	 * 
	 * @param clazz
	 */

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Atributo est�tico de tipo constantes de b�squeda para cambiar
	 * din�micamente la forma en la que el DAO busca
	 */

	public static ConstantesBusqueda constantesBusqueda;

	/**
	 * EntityManager inyectado por Spring
	 */

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * M�todo utilizado para obtener una criteria sobre la clase Persistente del
	 * DAO
	 * 
	 * @return criteria a utilizar
	 */

	protected Criteria getCriteria() {
		Session session = (Session) entityManager.getDelegate();
		return session.createCriteria(clazz);

	}

	/**
	 * M�todos Gen�ricos para ABMS
	 */

	@Transactional
	public void persist(T entity) {
		entityManager.persist(entity);

	}

	@Transactional
	public void persist(List<T> entities) {
		for (T t : entities) {
			persist(t);
		}

	}

	@Transactional
	public T update(T entity) {
		return entityManager.merge(entity);

	}

	/**
	 * Metodo utilizado para construir una criteria de una clase persistente
	 * dada
	 * 
	 * @param clazz
	 *            clase sobre la cual construir la criteria
	 * @return criteria para usar
	 */
	protected Criteria getCriteriaForClass(Class<T> clazz) {
		Session session = (Session) entityManager.getDelegate();
		return session.createCriteria(clazz);

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<T> findAll(T entity) {
		Criteria criteria = getCriteriaForClass((Class<T>) entity.getClass());
		return criteria.list();
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> findByHQL(String hql) {
		return entityManager.createQuery(hql).getResultList();
	}

	@Transactional
	public void remove(T entity) {
		entityManager.remove(entity);
	}

	@Transactional
	public void removeById(Long entityId) {
		T entity = findById(entityId);
		remove(entity);

	}

	public T findById(Long id) {
		return entityManager.find(clazz, id);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<T> findByExample(T exampleEntity) {
		Criteria criteria = getCriteriaForClass((Class<T>) exampleEntity
				.getClass());
		try {
			try {
				componerCriteria(criteria, exampleEntity);
			} catch (Exception e) {
				e.printStackTrace();
			}
			criteria.setProjection(null);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}

		if (criteria == null)
			return null;
		else
			return criteria.list();
	}

	/**
	 * M�todo para armar la criteria de un baseEntity
	 * 
	 * @param exampleEntity
	 * @param criteria
	 * @throws Exception
	 */

	@SuppressWarnings({ "static-access", "rawtypes" })
	private void armarCriteriaSinExample(Object exampleEntity, Criteria criteria)
			throws Exception {

		if (exampleEntity instanceof BaseEntity) {

			/**
			 * Armamos la criteria
			 */

			Class<?> clazz = exampleEntity.getClass();

			Method[] metodos = clazz.getMethods();

			for (int j = 0; j < metodos.length; j++) {
				if ((metodos[j].getName().substring(0, 3).equals("get"))
						&& (!metodos[j].getName().equals("getClass"))) {

					// Tengo un getter

					Method metodoDTO;
					metodoDTO = exampleEntity.getClass().getMethod(
							metodos[j].getName(), null);

					// Invocar el metodo en el filtro
					// Puede lanzar una InvocationTargetException
					Object valor = null;
					valor = metodoDTO.invoke(exampleEntity, null);
					if (valor != null) {
						Class rettypeDTO = metodoDTO.getReturnType();

						String s = metodos[j].getName().substring(3, 4)
								.toLowerCase()
								+ metodos[j].getName().substring(4);

						switch (rettypeDTO.getName()) {

						/**
						 * Si es String buscamos dependiendo de la busqueda
						 */
						case "java.lang.String":
							if (!constantesBusqueda.matchLike) {
								criteria.add(Restrictions.eq(s, (String) valor));
							} else {
								criteria.add(Restrictions.ilike(s,
										(String) valor, MatchMode.ANYWHERE));
							}

							break;

						/**
						 * Si es Date no comparamos horas, minutos y segundos
						 */
						case "java.util.Date":

							/**
							 * Creamos una fecha inicio y fin
							 */

							Calendar c = Calendar.getInstance();
							c.setTime((Date) valor);
							c.set(Calendar.HOUR_OF_DAY, 0);
							c.set(Calendar.MINUTE, 0);

							Date fechaInicio = c.getTime();

							c.set(Calendar.HOUR_OF_DAY, 24);

							Date fechaFin = c.getTime();

							criteria.add(Restrictions.between(s, fechaInicio,
									fechaFin));

							break;

						case "java.lang.Byte":
						case "java.lang.Short":
						case "java.lang.Integer":
						case "java.lang.Long": 
//						{
//							Long numero = new Long(valor.toString());
//							if (!(constantesBusqueda.ignoreZeros && numero == 0l)) {
//
//								criteria.add(Restrictions.eq(s, valor));
//
//							};
//							
//							
//							
//							break;
//						}
						case "java.lang.Float":
						case "java.lang.Double":
						case "java.lang.Character":
						case "java.lang.Boolean":
							criteria.add(Restrictions.eq(s, valor));
						}

					}
				}
			}

		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void componerCriteria(Criteria criteria, Object exampleEntity)
			throws Exception {
		Class<?> clazz = exampleEntity.getClass();

		// Si el ejemplo extiende de BaseEntity
		if (exampleEntity instanceof BaseEntity) {

			armarCriteriaSinExample(exampleEntity, criteria);

			// Obtengo los metodos del entity que extiendan de BaseEntity

			Method[] metodos = clazz.getMethods();

			for (int i = 0; i < metodos.length; i++) {

				// Determinar que el metodo sea un getter y que no sea el
				// getClass
				if (metodos[i].getName().substring(0, 3).equals("get")
						&& (!metodos[i].getName().equals("getClass"))) {

					Class retorno = metodos[i].getReturnType();

					// Obtengo el nombre de la propiedad a joinear
					String property = metodos[i].getName().substring(3, 4)
							.toLowerCase()
							+ metodos[i].getName().substring(4);

					// Si el metodo devuelve una Subclase de BaseEntity ->
					// RECURSIVIDAD para este entity

					if (BaseEntity.class.isAssignableFrom(retorno)) {

						// Obtengo el entity
						T entidad = (T) metodos[i].invoke(exampleEntity);

						if (entidad != null) {

							Criteria criteriaEntity = criteria.createCriteria(
									property, JoinType.INNER_JOIN);

							// recursividad
							componerCriteria(criteriaEntity, entidad);
						}

					} else if (Collection.class.isAssignableFrom(retorno)) {// si
																			// es
																			// una
																			// collection,
																			// obtengo
																			// el
																			// primer
																			// objeto
																			// y
																			// lo
																			// uso
																			// de
																			// ejemplo
						// Obtengo la coleccion
						Collection collection = (Collection) metodos[i]
								.invoke(exampleEntity);

						if (collection != null) {
							criteria.setFetchMode(property, FetchMode.JOIN);
							criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
							Criteria criteriaEntity = criteria.createCriteria(
									property, JoinType.LEFT_OUTER_JOIN);

							if (collection.size() > 0) {
								Iterator iterator = collection.iterator();
								Object entidad = iterator.next();

								// recursividad
								componerCriteria(criteriaEntity, entidad);
							}
						}

					}

				}

			}
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public PaginationResult<T> consultarPorFiltrosConPaginacion(
			T exampleEntity, int index, int offset) {

		Criteria criteria = getCriteriaForClass((Class<T>) exampleEntity
				.getClass());

		/**
		 * Busca generica
		 */

		try {
			try {
				componerCriteria(criteria, exampleEntity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		criteria.setProjection(null);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		/**
		 * Agregar desde y hasta
		 */

		if (index > 0)
			criteria.setFirstResult(index);
		if (offset > 0)
			criteria.setMaxResults(offset);

		/**
		 * Listar
		 */

		PaginationResult<T> resultado = new PaginationResult<T>();

		resultado.setListOfResult(criteria.list());

		/**
		 * Contar
		 */

		Criteria criteriaTotal = getCriteriaForClass((Class<T>) exampleEntity
				.getClass());

		try {
			try {
				componerCriteria(criteriaTotal, exampleEntity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}

		criteriaTotal.setProjection(Projections.rowCount());
		Number resultSize = (Number) criteriaTotal.uniqueResult();
		criteriaTotal.setProjection(null);

		resultado.setSize(resultSize.intValue());

		return resultado;
	}

	/**
	 * M�todo utilizado para devolver un objeto via reflection
	 */

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public T findOne(T entity) {
		Criteria criteria = getCriteriaForClass((Class<T>) entity.getClass());
		/**
		 * Busqueda generica
		 */
		try {
			componerCriteria(criteria, entity);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * Devolvemos un solo resultado
		 */
		T unico = (T) criteria.uniqueResult();

		return unico;
	}

	/**
	 * M�todo utilizado para obtener una conexi�n de JDBC desde Hibernate /
	 * Spring
	 */
	@Transactional
	public Connection getConnection() {
		Session session = entityManager.unwrap(Session.class);
		SessionFactoryImplementor sfi = (SessionFactoryImplementor) session
				.getSessionFactory();
		ConnectionProvider cp = sfi.getConnectionProvider();
		try {
			return cp.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<T> findAllByOrder(T entity, String campo, String order) {
		Criteria criteria = getCriteriaForClass((Class<T>) entity.getClass());

		try {
			componerCriteria(criteria, entity);
		} catch (Exception e) {
			e.printStackTrace();
		}

		criteria.setProjection(null);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

		if (order.equals("ASC")) {
			criteria.addOrder(Order.asc(campo));
		} else
			criteria.addOrder(Order.desc(campo));

		return criteria.list();
	}
}
