package ar.com.lemondata.apurate.dao.impl;

import org.springframework.stereotype.Repository;

import ar.com.lemondata.apurate.dao.GenericDAO;
import ar.com.lemondata.apurate.persistencia.BaseEntity;

/**
 * DAO gestionado por Spring para el uso generico de ABMCs
 */
@Repository
public class GenericDAOImpl extends BaseDAO<BaseEntity> implements GenericDAO {
	
	protected GenericDAOImpl() {
		super.setClazz(BaseEntity.class);
	}
}
