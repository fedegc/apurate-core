package ar.com.lemondata.apurate.datatransfer;

public abstract class BaseDTO {
	protected Long id;
	
	/**
	 * Metodo que devuelve el nombre del entity bean asociado
	 */
	public abstract String getEntityBeanName();	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseDTO other = (BaseDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
