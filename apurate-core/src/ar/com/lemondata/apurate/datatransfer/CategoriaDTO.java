package ar.com.lemondata.apurate.datatransfer;

public class CategoriaDTO extends BaseDTO {

	private String nombre;
	
	/**
	 * Empty Constructor
	 */
	public CategoriaDTO() {}
	
	public CategoriaDTO(String pNom) {
		nombre = pNom;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String getEntityBeanName() {
		return "Categoria";
	}

}
