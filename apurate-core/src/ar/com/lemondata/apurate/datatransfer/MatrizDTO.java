package ar.com.lemondata.apurate.datatransfer;

/**
 * 
 * @author Lemondata
 *
 */
public class MatrizDTO extends BaseDTO {

	private Integer dimension;
	private String valores;

	/**
	 * Empty Constructor
	 */
	public MatrizDTO() {}
	
	public MatrizDTO(Integer pDim, String pVals) {
		dimension = pDim;
		valores = pVals;
	}
	
	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public String getValores() {
		return valores;
	}

	public void setValores(String valores) {
		this.valores = valores;
	}

	@Override
	public String getEntityBeanName() {
		return "Matriz";
	}

}
