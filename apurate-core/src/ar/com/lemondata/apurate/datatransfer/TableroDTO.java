package ar.com.lemondata.apurate.datatransfer;

/**
 * 
 * @author Lemondata
 *
 */
public class TableroDTO extends BaseDTO {
	
	private MatrizDTO matriz;
	private CategoriaDTO categoria;
	
	/**
	 * Empty constructor
	 */
	public TableroDTO() {}
	
	public TableroDTO(MatrizDTO pMtz, CategoriaDTO pCat) {
		matriz = pMtz;
		categoria = pCat;
	}
	
	public MatrizDTO getMatriz() {
		return matriz;
	}

	public void setMatriz(MatrizDTO matriz) {
		this.matriz = matriz;
	}

	public CategoriaDTO getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaDTO categoria) {
		this.categoria = categoria;
	}

	@Override
	public String getEntityBeanName() {
		return "Tablero";
	}

}
