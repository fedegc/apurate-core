package ar.com.lemondata.apurate.datatransfer.request;

import java.util.List;


public class EventoDTORequest extends BaseDTORequest {
	
    private String nombre;
    private String fechaInicio;
    private String fechaFin;
    private Long canchaId;
    private Long deporteId;
    private Long organizador;
    private Long cupoMinimo;
    private Long cupoMaximo;
    private Long id;
    private List<Long> participanteList;
    private char estado;
    
    
    
    
    
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getFechaInicio() {
		return fechaInicio;
	}
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public Long getCanchaId() {
		return canchaId;
	}
	
	public void setCanchaId(Long canchaId) {
		this.canchaId = canchaId;
	}
	
	public Long getDeporteId() {
		return deporteId;
	}
	
	public void setDeporteId(Long deporteId) {
		this.deporteId = deporteId;
	}
	
	public Long getOrganizador() {
		return organizador;
	}
	
	public void setOrganizador(Long organizador) {
		this.organizador = organizador;
	}
	
	public Long getCupoMinimo() {
		return cupoMinimo;
	}
	
	public void setCupoMinimo(Long cupoMinimo) {
		this.cupoMinimo = cupoMinimo;
	}
	
	public Long getCupoMaximo() {
		return cupoMaximo;
	}
	
	public void setCupoMaximo(Long cupoMaximo) {
		this.cupoMaximo = cupoMaximo;
	}
	
	public List<Long> getParticipanteList() {
		return participanteList;
	}
	
	public void setParticipanteList(List<Long> participanteList) {
		this.participanteList = participanteList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
}