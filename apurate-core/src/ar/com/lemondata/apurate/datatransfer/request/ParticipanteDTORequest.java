package ar.com.lemondata.apurate.datatransfer.request;

import java.util.Date;

public class ParticipanteDTORequest extends BaseDTORequest {

	private Long id;
	private Date fechaInvitacion;
	private Character estado;
	private Long usuarioId;
	private Long eventoId;
	private Long clasificacionId;

	
	
	
	
	public Date getFechaInvitacion() {
		return fechaInvitacion;
	}

	public void setFechaInvitacion(Date fechaInvitacion) {
		this.fechaInvitacion = fechaInvitacion;
	}

	public Character getEstado() {
		return estado;
	}

	public void setEstado(Character estado) {
		this.estado = estado;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getEventoId() {
		return eventoId;
	}

	public void setEventoId(Long eventoId) {
		this.eventoId = eventoId;
	}

	public Long getClasificacionId() {
		return clasificacionId;
	}

	public void setClasificacionId(Long clasificacionId) {
		this.clasificacionId = clasificacionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}