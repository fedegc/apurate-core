package ar.com.lemondata.apurate.datatransfer.request;


public class TableroDTORequest extends BaseDTORequest {

	private Long id;
	private String categoria;	

	/**
	 * Empty Constructor
	 */
	public TableroDTORequest() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
}
