package ar.com.lemondata.apurate.datatransfer.request;

/**
 * DTO de usuario para serializar los datos de los usuarios
 */
public class UsuarioDTORequest extends BaseDTORequest {

	private Long id;
	private String username;
	private String password;
	private String correo;
	private Long telefono;
	private String nombre;
	private String apellido;
	private char estado;
	private char estadoAmistad; //Utilizado en el listado de amigos mobile
	private String claveGCM;

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Long getTelefono() {
		return telefono;
	}

	public void setTelefono(Long telefono) {
		this.telefono = telefono;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public char getEstadoAmistad() {
		return estadoAmistad;
	}

	public void setEstadoAmistad(char estadoAmistad) {
		this.estadoAmistad = estadoAmistad;
	}

	public String getClaveGCM() {
		return claveGCM;
	}

	public void setClaveGCM(String claveGCM) {
		this.claveGCM = claveGCM;
	}
	
	
	
}
