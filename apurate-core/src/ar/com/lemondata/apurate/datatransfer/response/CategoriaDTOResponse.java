package ar.com.lemondata.apurate.datatransfer.response;

public class CategoriaDTOResponse extends BaseDTOResponse {

	private String nombre;
	
	public CategoriaDTOResponse() {}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
