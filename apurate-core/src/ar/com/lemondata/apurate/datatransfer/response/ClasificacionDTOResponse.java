package ar.com.lemondata.apurate.datatransfer.response;

public class ClasificacionDTOResponse extends BaseDTOResponse {
    private String descripcion;
    private String alias;
    
    
    
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
    
    
}
