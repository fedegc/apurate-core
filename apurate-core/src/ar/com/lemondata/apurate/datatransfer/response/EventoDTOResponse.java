package ar.com.lemondata.apurate.datatransfer.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventoDTOResponse extends BaseDTOResponse {
	
	private Long id;
    private String nombre;
    private Date fechaInicio;
    private Date fechaFin;
	private Long cupoMinimo;
    private Long cupoMaximo;
    private char ganador;
    private DeporteDTOResponse deporteId;
    private CanchaDTOResponse canchaId;
    private UsuarioDTOResponse organizador;
    private List<ParticipanteDTOResponse> participanteList;
    private char estado;
	
    public EventoDTOResponse() {
    	this.deporteId = new DeporteDTOResponse();
		this.canchaId = new CanchaDTOResponse();
		this.organizador = new UsuarioDTOResponse();
		this.participanteList = new ArrayList<ParticipanteDTOResponse>();
    }
    
    
    public Long getId() {
		return id;
	}
	
    public void setId(Long id) {
		this.id = id;
	}
	
    public String getNombre() {
		return nombre;
	}
	
    public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
    public Date getFechaInicio() {
		return fechaInicio;
	}
	
    public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
    public Date getFechaFin() {
		return fechaFin;
	}
	
    public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
    public Long getCupoMaximo() {
		return cupoMaximo;
	}
	
    public void setCupoMaximo(Long cupoMaximo) {
		this.cupoMaximo = cupoMaximo;
	}
	
    public char getGanador() {
		return ganador;
	}
	
    public void setGanador(char ganador) {
		this.ganador = ganador;
	}
	
    public DeporteDTOResponse getDeporteId() {
		return deporteId;
	}
	
    public void setDeporteId(DeporteDTOResponse deporteId) {
		this.deporteId = deporteId;
	}
	
    public CanchaDTOResponse getCanchaId() {
		return canchaId;
	}
	
    public void setCanchaId(CanchaDTOResponse canchaId) {
		this.canchaId = canchaId;
	}
	
    public List<ParticipanteDTOResponse> getParticipanteList() {
		return participanteList;
	}
	
    public void setParticipanteList(List<ParticipanteDTOResponse> participanteList) {
		this.participanteList = participanteList;
	}

	public UsuarioDTOResponse getOrganizador() {
		return organizador;
	}

	public void setOrganizador(UsuarioDTOResponse organizador) {
		this.organizador = organizador;
	}

	public Long getCupoMinimo() {
		return cupoMinimo;
	}

	public void setCupoMinimo(Long cupoMinimo) {
		this.cupoMinimo = cupoMinimo;
	}


	public char getEstado() {
		return estado;
	}


	public void setEstado(char estado) {
		this.estado = estado;
	}

	
}
