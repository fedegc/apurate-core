package ar.com.lemondata.apurate.datatransfer.response;

public class MatrizDTOResponse extends BaseDTOResponse {

	private Integer dimension;
	private String valores;
	
	public MatrizDTOResponse() {}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public String getValores() {
		return valores;
	}

	public void setValores(String valores) {
		this.valores = valores;
	}

}
