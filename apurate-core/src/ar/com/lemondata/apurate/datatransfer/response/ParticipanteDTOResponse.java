package ar.com.lemondata.apurate.datatransfer.response;

import java.util.Date;

public class ParticipanteDTOResponse extends BaseDTOResponse {
	
	private Long id;
	private Date fechaInvitacion;
	private Character estado;
    private UsuarioDTOResponse usuarioId;
    private EventoDTOResponse eventoId;
    private ClasificacionDTOResponse clasificacionId;
	

    public ParticipanteDTOResponse() {
		this.eventoId = new EventoDTOResponse();
		this.usuarioId = new UsuarioDTOResponse();
		this.clasificacionId = new ClasificacionDTOResponse();
    }




	public Date getFechaInvitacion() {
		return fechaInvitacion;
	}

	public void setFechaInvitacion(Date fechaInvitacion) {
		this.fechaInvitacion = fechaInvitacion;
	}

	public Character getEstado() {
		return estado;
	}

	public void setEstado(Character estado) {
		this.estado = estado;
	}

	public UsuarioDTOResponse getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(UsuarioDTOResponse usuarioId) {
		this.usuarioId = usuarioId;
	}

	public EventoDTOResponse getEventoId() {
		return eventoId;
	}

	public void setEventoId(EventoDTOResponse eventoId) {
		this.eventoId = eventoId;
	}

	public ClasificacionDTOResponse getClasificacionId() {
		return clasificacionId;
	}

	public void setClasificacionId(ClasificacionDTOResponse clasificacionId) {
		this.clasificacionId = clasificacionId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
   
}