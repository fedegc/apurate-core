package ar.com.lemondata.apurate.datatransfer.response;

public class ResponseWSDTO {
	
	private int codigo;
	private String mensaje;
	private Object objeto;
	private String metodo; //Utilizado para identificar en mobile de que m�todo es la respuesta
	
	/** 			GETTERS & SETTERS 			**/
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Object getObjeto() {
		return objeto;
	}
	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	
}
