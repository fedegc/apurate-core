package ar.com.lemondata.apurate.datatransfer.response;


public class TableroDTOResponse extends BaseDTOResponse {

	private MatrizDTOResponse matriz;
	private CategoriaDTOResponse categoria;

	public TableroDTOResponse() {}

	public MatrizDTOResponse getMatriz() {
		return matriz;
	}

	public void setMatriz(MatrizDTOResponse matriz) {
		this.matriz = matriz;
	}

	public CategoriaDTOResponse getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaDTOResponse categoria) {
		this.categoria = categoria;
	}
	
}
