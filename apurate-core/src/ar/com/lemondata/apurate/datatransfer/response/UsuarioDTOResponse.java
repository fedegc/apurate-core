package ar.com.lemondata.apurate.datatransfer.response;


public class UsuarioDTOResponse extends BaseDTOResponse {

	private Long id;
	private String username;
	private String nombre;
	private String apellido;
	private char estado;
	private String correo;
	private String domicilio;	
	private char estadoAmistad; //Utilizado en el listado de amigos mobile

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public char getEstadoAmistad() {
		return estadoAmistad;
	}

	public void setEstadoAmistad(char estadoAmistad) {
		this.estadoAmistad = estadoAmistad;
	}
	
	

}
