package ar.com.lemondata.apurate.mapper;

import ar.com.lemondata.apurate.datatransfer.BaseDTO;
import ar.com.lemondata.apurate.datatransfer.request.BaseDTORequest;
import ar.com.lemondata.apurate.datatransfer.response.BaseDTOResponse;

public abstract class BaseMapper {

	public abstract BaseDTO mapRequest(BaseDTORequest request);
	
	public abstract BaseDTOResponse mapResponse(BaseDTO response);
}
