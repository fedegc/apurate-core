package ar.com.lemondata.apurate.mapper;

import ar.com.lemondata.apurate.datatransfer.BaseDTO;
import ar.com.lemondata.apurate.datatransfer.TableroDTO;
import ar.com.lemondata.apurate.datatransfer.request.BaseDTORequest;
import ar.com.lemondata.apurate.datatransfer.request.TableroDTORequest;
import ar.com.lemondata.apurate.datatransfer.response.BaseDTOResponse;
import ar.com.lemondata.apurate.datatransfer.response.TableroDTOResponse;
import ar.com.lemondata.apurate.utillity.DozerUtil;

public class TableroMapper extends BaseMapper {

	@Override
	public BaseDTO mapRequest(BaseDTORequest request) {
		return (TableroDTO) DozerUtil.map((TableroDTORequest)request, TableroDTO.class);
	}

	@Override
	public BaseDTOResponse mapResponse(BaseDTO response) {
		return (TableroDTOResponse) DozerUtil.map((TableroDTO)response, TableroDTOResponse.class);
	}

}
