package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * 
 * @author Lemondata
 *
 */
@Entity
@Table(name = "desafio")
public class Desafio extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9089257448525332287L;
	
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue
	@Basic(optional = false)
	private Long id;

	@JoinColumn(name = "usuario_desafiante_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = false)
	private Usuario desafiante;
	
	@JoinColumn(name = "usuario_desafiado_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = false)
	private Usuario desafiado;	
	
	@JoinColumn(name = "juego_desafiante_id", referencedColumnName = "id", nullable = true)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = true)
	private Juego juegoDesafiante;

	@JoinColumn(name = "juego_desafiado_id", referencedColumnName = "id", nullable = true)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = true)
	private Juego juegoDesafiado;
	
	@JoinColumn(name = "usuario_ganador_id", referencedColumnName = "id", nullable = true)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = true)
	private Usuario ganador;
	
	@Column(name = "activo", nullable = false)
	@Type(type = "yes_no")
	@Basic(optional = false)
	private boolean activo;
	
	public Desafio() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Juego getJuegoDesafiante() {
		return juegoDesafiante;
	}

	public void setJuegoDesafiante(Juego juegoDesafiante) {
		this.juegoDesafiante = juegoDesafiante;
	}

	public Juego getJuegoDesafiado() {
		return juegoDesafiado;
	}

	public void setJuegoDesafiado(Juego juegoDesafiado) {
		this.juegoDesafiado = juegoDesafiado;
	}

	public Usuario getGanador() {
		return ganador;
	}

	public void setGanador(Usuario ganador) {
		this.ganador = ganador;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

}
