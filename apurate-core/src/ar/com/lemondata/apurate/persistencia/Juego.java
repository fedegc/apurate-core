package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Lemondata
 *
 */
@Entity
@Table(name = "juego")
public class Juego extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4570718220584873454L;

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	@Basic(optional = false)
	private Long id;

	@Column(name = "puntaje", nullable = false)
	@Basic(optional = false)
	private Long puntaje;

	@JoinColumn(name ="usuario_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = false)
	private Usuario usuario;

	@JoinColumn(name = "modo_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = false)
	private ModoJuego modo;

	@JoinColumn(name = "tablero_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@Basic(optional = false)
	private Tablero tablero;
	
	public Juego() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Long puntaje) {
		this.puntaje = puntaje;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public ModoJuego getModo() {
		return modo;
	}

	public void setModo(ModoJuego modo) {
		this.modo = modo;
	}

	public Tablero getTablero() {
		return tablero;
	}

	public void setTablero(Tablero tablero) {
		this.tablero = tablero;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
