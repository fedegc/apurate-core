package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "matriz")
public class Matriz extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 511175277023008860L;
	
	@Id
	@GeneratedValue
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;
	
	@Basic(optional = false)
	@Column(name = "dimension")
	private int dimension;
	
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 1024)
	@Column(name = "valores")
	private String valores;
	
	public Matriz() {
		// TODO Auto-generated constructor stub
	}
	
	public Matriz(Long pId, Integer pDimension, String pValores) {
		this.id=pId;
		this.dimension=pDimension;
		this.valores=pValores;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public String getValores() {
		return valores;
	}

	public void setValores(String valores) {
		this.valores = valores;
	}
	
	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Matriz)) {
			return false;
		}
		Matriz other = (Matriz) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ar.com.lemondata.apurate.persistencia.Matriz[ id=" + id + " ]";
	}

}
