package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Lemondata
 *
 */
@Entity
@Table(name = "modo_de_juego")
public class ModoJuego extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7978913289646302149L;

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue
	@Basic(optional = false)
	private Long id;
	
	@Column(name = "nombre", nullable = false, length = 128)
	@Basic(optional = false)
	private String nombre;
	
	public ModoJuego() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
