package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "palabra")
public class Palabra extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5306613987386267730L;

	@Id
	@GeneratedValue
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 45)
	@Column(name = "texto")
	private String texto;

	@Basic(optional = false)
	@Column(name = "cantidad_letras")
	private Long cantidadLetras;

	@JoinColumn(name = "categoria_id", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Categoria categoria;
	
	
	public Palabra() {

	}

	public Palabra(Long pId, String pTexto, Long pCantidadLetras, Categoria pCat) {
		this.id = pId;
		this.texto = pTexto;
		this.cantidadLetras = pCantidadLetras;
		this.categoria = pCat;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Long getCantidadLetras() {
		return cantidadLetras;
	}

	public void setCantidadLetras(Long cantidadLetras) {
		this.cantidadLetras = cantidadLetras;
	}
	
	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Palabra)) {
			return false;
		}
		Palabra other = (Palabra) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ar.com.lemondata.apurate.persistencia.Palabra[ id=" + id + " ]";
	}

}
