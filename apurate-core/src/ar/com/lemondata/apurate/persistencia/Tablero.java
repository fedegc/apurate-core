package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tablero")
public class Tablero extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4044668367229964680L;
	
	@Id
	@GeneratedValue
	@Basic(optional = false)
	@Column(name = "id")
	private Long id;
	
	@JoinColumn(name = "matriz_id", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Matriz matriz;
	
	@JoinColumn(name = "categoria_id", referencedColumnName = "id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Categoria categoria;

	public Tablero(Long pId, Matriz pMatriz, Categoria pCategoria) {
		this.id=pId;
		this.matriz=pMatriz;
		this.categoria=pCategoria;
		
	}
	
	public Tablero() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Matriz getMatriz() {
		return matriz;
	}

	public void setMatriz(Matriz matriz) {
		this.matriz = matriz;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Tablero)) {
			return false;
		}
		Tablero other = (Tablero) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ar.com.lemondata.apurate.persistencia.Tablero[ id=" + id + " ]";
	}

	
}
