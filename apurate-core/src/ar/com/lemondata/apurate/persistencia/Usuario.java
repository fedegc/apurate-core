package ar.com.lemondata.apurate.persistencia;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Clase de un usuario.
 * @author Lemondata
 *
 */
@Entity
@Table(name = "usuario")
public class Usuario extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3200144394692950852L;

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	@Basic(optional = false)
	private Long id;
	
	@Column(name = "nombre", nullable = false)
	@Basic(optional = false)
	private String nombre;
	
	@Column(name = "password", nullable = false)
	@Basic(optional = false)
	private String password;
	
	@Column(name = "mail", nullable = false)
	@Basic(optional = false)
	private String mail;
	
	@Column(name = "cuenta", nullable = false)
	@Basic(optional = false)
	private String cuenta;
	
	public Usuario() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
}
