package ar.com.lemondata.apurate.service;

import java.util.List;

import ar.com.lemondata.apurate.datatransfer.BaseDTO;
import ar.com.lemondata.apurate.utillity.PaginationResult;

public interface GenericService {
	
	void persist(BaseDTO baseDTO);

	BaseDTO persistAndReturn(BaseDTO baseDTO);

	void update(BaseDTO baseDTO);

	void remove(BaseDTO baseDTO);

	List<BaseDTO> findAll(BaseDTO baseDTO);

	List<BaseDTO> findAllByExample(BaseDTO baseDTO);

	List<BaseDTO> findAllByOrder(BaseDTO baseDTO, String campo, String orden);
	
	BaseDTO findOne(BaseDTO baseDTO);

	PaginationResult<BaseDTO> consultarPorFiltrosConPaginacion(BaseDTO filtro,
			int index, int offset);
}
