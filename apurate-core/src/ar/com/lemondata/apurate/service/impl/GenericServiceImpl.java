package ar.com.lemondata.apurate.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.com.lemondata.apurate.dao.GenericDAO;
import ar.com.lemondata.apurate.datatransfer.BaseDTO;
import ar.com.lemondata.apurate.persistencia.BaseEntity;
import ar.com.lemondata.apurate.service.GenericService;
import ar.com.lemondata.apurate.utillity.ConverterUtil;
import ar.com.lemondata.apurate.utillity.DozerUtil;
import ar.com.lemondata.apurate.utillity.PaginationResult;

@SuppressWarnings("rawtypes")
@Service
public class GenericServiceImpl implements GenericService {

	/**
	 * Inyectamos el GenericDAO
	 */
	@Resource
	private GenericDAO genericDao;
	
	@Transactional
	public void persist(BaseDTO baseDTO) {

		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		/**
		 * Convertimos a la entidad necesaria
		 */

		BaseEntity be = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Persistimos la entidad
		 */

		genericDao.persist(be);

	}

	@Transactional
	public BaseDTO persistAndReturn(BaseDTO baseDTO) {

		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		/**
		 * Convertimos a la entidad necesaria
		 */

		BaseEntity be = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Persistimos la entidad
		 */

		genericDao.persist(be);

		BaseDTO dto = (BaseDTO) DozerUtil.map(be, baseDTO.getClass());
		return dto;

	}

	@Transactional
	public void update(BaseDTO baseDTO) {

		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		/**
		 * Convertimos a la entidad necesaria
		 */

		BaseEntity be = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Persistimos la entidad
		 */

		genericDao.update(be);

	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Transactional
	public void remove(BaseDTO baseDTO) {
		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		/**
		 * Convertimos a la entidad necesaria
		 */

		BaseEntity be = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Setamos la clase persistente
		 */

		genericDao.setClazz(c);
		/**
		 * Persistimos la entidad
		 */

		genericDao.removeById(baseDTO.getId());

	}

	public List<BaseDTO> findAll(BaseDTO baseDTO) {
		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		BaseEntity baseEntity = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Creamos la coleccion a devolver
		 */
		List<BaseDTO> listaDTOs = new ArrayList<BaseDTO>();

		/**
		 * Hacemos la consulta
		 */

		List<BaseEntity> listaEntityes = genericDao.findAll(baseEntity);

		/**
		 * Convertimos a DTOs
		 */

		for (BaseEntity be : listaEntityes) {
			BaseDTO bDTO = (BaseDTO) DozerUtil.map(be, baseDTO.getClass());
			listaDTOs.add(bDTO);

		}
		return listaDTOs;
	}

	@Transactional
	public PaginationResult<BaseDTO> consultarPorFiltrosConPaginacion(
			BaseDTO filtro, int index, int offset) {

		PaginationResult<BaseDTO> resultado = new PaginationResult<BaseDTO>();

		Class c = ConverterUtil.mapEntityToClass(filtro.getEntityBeanName());

		BaseEntity baseEntity = (BaseEntity) DozerUtil.map(filtro, c);

		PaginationResult<BaseEntity> resultadoEntityes = genericDao
				.consultarPorFiltrosConPaginacion(baseEntity, index, offset);

		resultado.setSize(resultadoEntityes.getSize());

		List<BaseDTO> lista = new ArrayList<BaseDTO>();

		for (BaseEntity entity : resultadoEntityes.getListOfResult()) {
			BaseDTO dto = (BaseDTO) DozerUtil.map(entity, filtro.getClass());
			lista.add(dto);

		}

		resultado.setListOfResult(lista);

		return resultado;
	}

	@Override
	public BaseDTO findOne(BaseDTO baseDTO) {

		/** Armamos el BaseEntity */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());
		BaseEntity baseEntity = (BaseEntity) DozerUtil.map(baseDTO, c);

		/** Buscar el ejemplo */
		BaseEntity resultado = genericDao.findOne(baseEntity);

		/** Devolver el DTO correspondiente */
		BaseDTO resultadoDTO = null;

		if (resultado != null)
			resultadoDTO = (BaseDTO) DozerUtil.map(resultado, baseDTO.getClass());

		return resultadoDTO;
	}

	@Override
	public List<BaseDTO> findAllByExample(BaseDTO baseDTO) {
		/** Obtenemos el nombre del entity bean asociado  */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		BaseEntity baseEntity = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Creamos la coleccion a devolver
		 */
		List<BaseDTO> listaDTOs = new ArrayList<BaseDTO>();

		/**
		 * Hacemos la consulta
		 */

		List<BaseEntity> listaEntityes = genericDao.findByExample(baseEntity);

		/**
		 * Convertimos a DTOs
		 */

		for (BaseEntity be : listaEntityes) {
			BaseDTO bDTO = (BaseDTO) DozerUtil.map(be, baseDTO.getClass());
			listaDTOs.add(bDTO);

		}
		return listaDTOs;
	}

	@Override
	public List<BaseDTO> findAllByOrder(BaseDTO baseDTO, String campo,
			String orden) {

		/**
		 * Obtenemos el nombre del entity bean asociado
		 */
		Class c = ConverterUtil.mapEntityToClass(baseDTO.getEntityBeanName());

		BaseEntity baseEntity = (BaseEntity) DozerUtil.map(baseDTO, c);

		/**
		 * Creamos la coleccion a devolver
		 */
		List<BaseDTO> listaDTOs = new ArrayList<BaseDTO>();

		/**
		 * Hacemos la consulta
		 */

		List<BaseEntity> listaEntityes = genericDao.findAllByOrder(baseEntity,
				campo, orden);

		/**
		 * Convertimos a DTOs
		 */

		for (BaseEntity be : listaEntityes) {
			BaseDTO bDTO = (BaseDTO) DozerUtil.map(be, baseDTO.getClass());
			listaDTOs.add(bDTO);

		}
		return listaDTOs;
	}	
	
}