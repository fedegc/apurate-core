package ar.com.lemondata.apurate.utillity;

public class ConstantesApurate {
	//Constantes de usuarios
	public static final char USUARIO_ESTADO_ACTIVO = 'A';
	
	//Constantes de amigos
	public static final char AMIGO_ESTADO_PENDIENTE = 'P';
	public static final char AMIGO_ESTADO_PENDIENTE_ACEPTAR = 'I';
	public static final char AMIGO_ESTADO_CONFIRMADO = 'A';
	public static final char AMIGO_ESTADO_RECHAZADO = 'R';	
	
	//Constantes eventos
	public static final char INVITACION_EVENTO_PENDIENTE = 'P';
	public static final char INVITACION_EVENTO_ACEPTADA = 'A';
	public static final char INVITACION_EVENTO_RECHAZADA = 'R';
	
	public static final char EVENTO_GANADOR_SIN_ESTABLECER = 'N';
	
	public static final char EVENTO_ESTADO_PENDIENTE = 'P';	
	public static final char EVENTO_ESTADO_CONFIRMADO = 'A';
	public static final char EVENTO_ESTADO_RECHAZADO = 'R';	
	
	//Constantes de resultados de WS
	public static final int RESULTADO_EXITO = 0;
	public static final int RESULTADO_USUARIO_INEXISTENTE = 1;
	public static final int RESULTADO_USUARIO_INHABILITADO = 2;
	public static final int RESULTADO_ERROR = -1;
	
	//Tipos de mensajes
	public static final int TIPO_MENSAJE_NUEVO_AMIGO = 1;
	public static final int TIPO_MENSAJE_NUEVO_EVENTO = 2;
}
