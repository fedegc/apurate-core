package ar.com.lemondata.apurate.utillity;

/**
 * Clase que define constantes que seran utilizadas
 * por los DAO genericos para realizar consultas
 * @author N�stor
 *
 */
public class ConstantesBusqueda {
	
	/**
	 * Constante utilizada para buscar Strings por like o ilike
	 */
	public static Boolean matchLike;
	
	/**
	 * Constante utilizada para ignorar los 0
	 */
	public static Boolean ignoreZeros;

}
