package ar.com.lemondata.apurate.utillity;

/**
 * Clase Utilizada para realizar conversiones entre clases
 * @author nestor
 *
 */

public class ConverterUtil {

public static final String PACKAGE_PREFIX = "ar.com.lemondata.apurate.persistencia.";
	
	public static final String PACKAGE_DTO_PREFIX = "ar.com.lemondata.apurate.datatransfer.";
	
	@SuppressWarnings("rawtypes")
	public static Class mapEntityToClass(String className) {
		Class c = null;
		StringBuffer canonicalName = new StringBuffer(PACKAGE_PREFIX);
		canonicalName.append(className);
		try {
			c = Class.forName(canonicalName.toString());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return c;
	}
}
