package ar.com.lemondata.apurate.utillity;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 * Clase util para mapear desde un entity a DTO
 */
public class DozerUtil {

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object map(Object sourceObject, Class clazz) {
		Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
		return mapper.map(sourceObject, clazz);
	}
}

