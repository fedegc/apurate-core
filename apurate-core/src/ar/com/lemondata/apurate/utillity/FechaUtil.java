package ar.com.lemondata.apurate.utillity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FechaUtil {

	public static String formatDate(Date date) {
		//TODO::crear una clase de constantes
			String mascaraFecha = "dd/MM/yyyy";
			SimpleDateFormat sdf = new SimpleDateFormat(mascaraFecha);
			return sdf.format(date);
	}
}
