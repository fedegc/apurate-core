package ar.com.lemondata.apurate.utillity;

public class MensajesApurate {

	//Registro y Login
	public static final String msj_error_registro = "Ocurri\u00F3 un error en el registro, por favor int\u00E9ntelo m\u00E1s tarde.";
	public static final String msj_usuario_deshabilitado = "Su cuenta est\u00E1 deshabilitada.";
	public static final String msj_usuario_incorrecto = "Usuario o contrase\u00F1a incorrectos.";
	public static final String msj_validacion_registro = "Por favor complete todos los campos obligatorios.";
	public static final String msj_error_inesperado = "Ocurri\u00F3 un error inesperado.";
	public static final String msj_usuario_existente = "El nombre de usuario ingresado ya existe.";
	public static final String msj_mail_existente = "El correo ingresado ya existe.";
	
	//General
	public static final String msj_error_listado = "Ocurri\u00F3 un error al obtener el listado.";
	
	//Eventos
	public static final String MSJ_INVITACION_EVENTO_TITLE = "Nueva Invitaci\u00F3n.";
	public static final String MSJ_INVITACION_EVENTO = "Tiene una nueva invitaci\u00F3n a un partido.";
	
	public static final String MSJ_EVENTO_CONFIRMADO_TITLE = "PARTIDO CONFIRMADO.";
	public static final String MSJ_EVENTO_CONFIRMADO = "Se confirmo el partido ";	
	
	public static final String MSJ_EVENTO_CANCELADO_TITLE = "PARTIDO CANCELADO.";
	public static final String MSJ_EVENTO_CANCELADO = "Se cancelo el partido ";	
	
	public static final String MSJ_EVENTO_ACTUALIZADO_TITLE = "PARTIDO ACTUALIZADO.";
	public static final String MSJ_EVENTO_ACTUALIZADO = "Se actualizo el partido ";		
	
	//Amigos
	public static final String MSJ_SOLICITUD_AMISTAD_TITLE = "Solicitud de amistad.";
	public static final String MSJ_SOLICITUD_AMISTAD = "Tiene una nueva solicitud de amistad.";
	
	public static final String MSJ_CONFIRMACION_AMISTAD_TITLE = "Confirmaci\u00F3n de amistad.";
	public static final String MSJ_CONFIRMACION_AMISTAD = " acepto tu solicitud de amistad.";
	
	public static final String MSJ_AMISTAD_EXISTENTE = "Ya eres amigo del usuario seleccionado.";
	public static final String MSJ_AMISTAD_PENDIENTE = "Ya existe una solicitud de amistad para el usuario seleccionado.";
	
	//Usuarios
	public static final String MSJ_USUARIO_NO_ENCONTRADO = "No se encontro el usuario solicitado.";
	
}
