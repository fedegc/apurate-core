package ar.com.lemondata.apurate.utillity;

/**
 * 
 * @author Nestor
 * 
 *         Interface utilizada para especificar como un DTO se debe ordenar
 */

public interface Ordenable {

	String getOrden();

	String getModoOrden();
}
