package ar.com.lemondata.apurate.utillity;

import java.util.Collection;

/**
 * Clase utilizada para pasar listados paginados entre capas
 * 
 * @author N�stor
 * 
 * @param <T>
 */
public class PaginationResult<T> {

	private Collection<T> listOfResult;

	private int size;

	public Collection<T> getListOfResult() {
		return listOfResult;
	}

	public void setListOfResult(Collection<T> listOfResult) {
		this.listOfResult = listOfResult;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}