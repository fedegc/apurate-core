package ar.com.lemondata.apurate.utillity;

/**
 * Interface que deberan implementar los DTO
 * para poder ser seleccionados de una lista
 * @author N�stor
 *
 */
public interface Seleccionable {
	
	Long getId();
	
	String getDescripcion();

}
