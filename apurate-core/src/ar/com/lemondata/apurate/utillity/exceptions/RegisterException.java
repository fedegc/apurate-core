package ar.com.lemondata.apurate.utillity.exceptions;

public class RegisterException extends Exception {
	private static final long serialVersionUID = 4571260075414630755L;

	public RegisterException(){
		
	}
	
	public RegisterException(String mensaje){
		super(mensaje);
	}
}
