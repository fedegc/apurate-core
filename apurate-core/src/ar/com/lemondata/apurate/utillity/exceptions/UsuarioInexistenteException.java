package ar.com.lemondata.apurate.utillity.exceptions;

public class UsuarioInexistenteException extends Exception {
	private static final long serialVersionUID = -6600999293063057629L;

	public UsuarioInexistenteException() {
	}	

	public UsuarioInexistenteException(String mensaje) {
		super(mensaje);
	}

}
