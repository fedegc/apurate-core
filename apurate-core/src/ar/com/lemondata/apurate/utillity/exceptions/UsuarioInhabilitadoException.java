package ar.com.lemondata.apurate.utillity.exceptions;

public class UsuarioInhabilitadoException extends Exception {
	private static final long serialVersionUID = -1776942529929112625L;

	public UsuarioInhabilitadoException() {
		
	}

	public UsuarioInhabilitadoException(String message) {
		super(message);
	}

	
}
